const divStyle = {
    width: "120px",
    height: "40px",
    backgroundColor: "rgb(189, 252, 252)",
    color: "rgb(57, 40, 140)",
    border: "solid 1px",
    borderRadius: "10px",
    fontSize: "20px",
    lineHeight: "20px",
    paddingLeft: "10px",
    paddingTop: "10px",
    cursor: "pointer",
};

function Button2() {
    return <div style={divStyle}>Подтвердить</div>;
}

export default Button2;